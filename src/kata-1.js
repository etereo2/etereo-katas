export function kata1(number) {
    return main(number);
}

function numberToArray(number) {
    let array = number.toString().split('');
    return array.map(x => parseInt(x));
}

function main(number) {

    const numberArray = numberToArray(number)
    numberArray.sort(function (d1, d2) {
        return d2 - d1
    });

    return numberArray.join('');
}


