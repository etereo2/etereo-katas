export function kata3(n, m) {
    return main(n, m);
}

function main(n, m) {
    const result = [];

    for (let i = n; i <= m; i++) {
        const squareDivisorsSum = sumOfSquareDivisorsIsSquarePerfect(i);
        if (squareDivisorsSum) {
            result.push([i, squareDivisorsSum]);
        }
    }

    return result;
};

function getDivisors(n) {
    const divisors = [];
    for (let i = 1; i <= n; i++) {
        if (n % i === 0) {
            divisors.push(i);
        }
    }
    return divisors;
}


function sumOfSquareDivisorsIsSquarePerfect(n) {
    const divisors = getDivisors(n);
    const squareDivisors = divisors.map(n => n * n);
    const squareDivisorsSum = squareDivisors.reduce((accum, next) => accum + next, 0);

    if (Number.isInteger(Math.sqrt(squareDivisorsSum))) {
        return squareDivisorsSum;
    }

    return false;
}
