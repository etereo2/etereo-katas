export function kata2(obj, def, path) {
    return main(obj, def, path);
}

function main(obj, def, path) {
    if (path) {
        const pathArray = path.split('.');
        const reducer = (obj, key) => {
            if (obj !== null && obj[key]) {
                return obj[key];
            }
            return null;
        };
        const value = pathArray.reduce(reducer, obj);
        return (value === null && def !== null) ? def : value;
    }
    else {
        return (path) => main(obj, def, path);
    }
}